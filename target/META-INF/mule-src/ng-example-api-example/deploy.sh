
echo ----------------------
echo Starting Build
echo ----------------------
jenv local openjdk64-1.8.0.222
jenv_set_java_home
mvn clean 



echo ----------------------
echo Starting Testing and Code Scan
echo ----------------------

jenv local openjdk64-11.0.6
jenv_set_java_home
mvn clean test sonar:sonar -DskipTests -Dencrypt.key=secure12345 -Denv=dev    

echo ----------------------
echo Starting Deployment
echo ----------------------
jenv local openjdk64-1.8.0.222
jenv_set_java_home

echo "******* Increment Release Version in POM ******** \n"
mvn release:update-versions

echo ******* Deploying Code to Exchange ********
mvn clean deploy -Dencrypt.key=secure12345 -Denv=dev -DSkipTests

echo ******* Deploying Code to RTF ********

mvn -DmuleDeploy -P rtf deploy \                                                                         
-Dap.client_id=dceabcf58e9a4441b41e691844b63571 \
-Dap.client_secret=30BC078B520A481C8990E460a73aECfD \
-Dencrypt.key=secure12345 \
-Dap.businessGroup="Customer Operations and Services" \
-Druntime.fabric.name="Azure non-prod" \
-Drtf.lastMile=true \
-Drtf.clustering=false \
-Drtf.replication.factor=1 \
-Drtf.cpu.res=2000m \
-Drtf.cpu.max=2000m \
-Drtf.memory.res=1000mi \
-Ddeployment.env="dev" \
-Dap.environment="Dev-INT-RTF-OFM"