%dw 2.0
output application/json
---
{
  status: vars.httpStatus,
  payload: {
    customerId: "ABC123",
    firstName: "John",
    lastName: "Smith"
  },
  errors: []
}