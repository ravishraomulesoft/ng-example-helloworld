%dw 2.0
output application/json
---
{
	status: payload.status default 0,
	payload: vars.errorPayload default "message": "Error",
	errors: payload.errors default []
}